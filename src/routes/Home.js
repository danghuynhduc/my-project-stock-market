import React from "react";
import StocksSpotlight from "../components/DailySpotlight";

const Markets = () => {
  return (
    <div style={{ marginTop: "8rem" }}>
      <StocksSpotlight />
    </div>
  );
};

export default Markets;
