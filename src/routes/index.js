import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import Home from "./Home";
import SearchTraningCompany from "./SearchTraningCompany";
import Predictdashboard from "./Predictdashboard";

import NotFound from "../components/NotFound";

//---------------------------------------------------
const Router = () => (
  <Switch>
    {/* ROOT */}
    {/* HOME */}
    <Route exact path="/" component={Home} />
    <Route
      exact
      path="/SearchTraningCompany"
      component={SearchTraningCompany}
    />
    <Route
      exact
      path="/predictdashboard"
      component={Predictdashboard}
    />

    <Route component={NotFound} />
  </Switch>
);

export default Router;
