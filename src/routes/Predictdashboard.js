import React, { Component} from "react";
import SelectModel from "../components/SelectModel";
import PredictField from "../components/PredictField";
import { csv } from 'd3-request';
import ReactApexChart from "react-apexcharts";
// import url from ' /coca-cola.csv';
import url from '../data/coca-cola.csv';

class Predictdashboard extends Component {
  
  state = {
    predictFuture:0,
    scoreMSE: 0,
    scoreRMSE: 0,
    currentTime:2,
    error: null,
    enteredSymbol: "aapl",
    quote: null,
    quoteHistory: [],
    showHistory: false,
    news: [],
    showAllNews: false,
    chart: [],
    showAllChart: false,
    series: [{
      name: "Session Duration",
      data: [45, 52, 38, 24, 33, 26, 21, 20, 6, 8, 15, 10]
    },
    {
      name: "Page Views",
      data: [35, 41, 62, 42, 13, 18, 29, 37, 36, 51, 32, 35]
    },
    // {
    //   name: 'Total Visits',
    //   data: [87, 57, 74, 99, 75, 38, 62, 47, 82, 56, 45, 47]
    // }
  ],
  options: {
    chart: {
      height: 400,
      type: 'area',
      zoom: {
        type: 'x',
          enabled: true,
          autoScaleYaxis: true
      },
      toolbar: {
        autoSelected: 'zoom'
      }
    },
    dataLabels: {
      enabled: false
    },
    // stroke: {
    //   width: [5, 7, 5],
    //   curve: 'straight',
    //   dashArray: [0, 8, 5]
    // },
    title: {
      text: 'Stock Price Movement',
      align: 'left'
    },
    legend: {
      tooltipHoverFormatter: function(val, opts) {
        return val + ' - ' + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + ''
      }
    },
    markers: {
      size: 0,
      hover: {
        sizeOffset: 6
      }
    },
    yaxis: {
      labels: {
        formatter: function (val) {
          return (val);
        },
      },
      title: {
        text: 'Price'
      },
    },
    xaxis: {
      categories: ['01 Jan', '02 Jan', '03 Jan', '04 Jan', '05 Jan', '06 Jan', '07 Jan', '08 Jan', '09 Jan',
      '10 Jan', '11 Jan', '12 Jan'
    ],
      type: 'datetime',
    },
    tooltip: {
      y: [
        {
          title: {
            formatter: function (val) {
              return val + " (mins)"
            }
          }
        },
        {
          title: {
            formatter: function (val) {
              return val + " per session"
            }
          }
        }
      ]
    },
    grid: {
      borderColor: '#f1f1f1',
    }
  },
}

SendResquest = (model) => {
  const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ 'model':model })
  };
  fetch('/choosemodel', requestOptions)
      .then(response => response.json())
      .then(
        (result) => {
          // this.setState({
          //   series: [{
          //     name: "Real Price",
          //     data: result.realPrice
          //   },
          //   {
          //     name: "Predict Price",
          //     data: result.predictPrice
          //   }],
          //   currentTime:result.time,
          //   xaxis: {
          //     categories: result.date,
          //     type: 'datetime',
          //   },
          // });
          this.setState({
            scoreMSE: result.scoreTestMSE,
            scoreRMSE: result.scoreTestRMSE,
            series: [
                    {
                      name: "Predict Price",
                      data: result.predictPrice
                    },
                    {
                      name: "Real Price",
                              data: result.realPrice
                     },
            // {
            //   name: 'Total Visits',
            //   data: [87, 57, 74, 99, 75, 38, 62, 47, 82, 56, 45, 47]
            // }
          ],
          options: {
            chart: {
              height: 400,
              type: 'area',
              zoom: {
                type: 'x',
                  enabled: true,
                  autoScaleYaxis: true
              },
              toolbar: {
                autoSelected: 'zoom'
              }
            },
            dataLabels: {
              enabled: false
            },
            // stroke: {
            //   width: [5, 7, 5],
            //   curve: 'straight',
            //   dashArray: [0, 8, 5]
            // },
            title: {
              text: 'Stock Price Movement of Coca-Cola Company',
              align: 'left'
            },
            legend: {
              tooltipHoverFormatter: function(val, opts) {
                return val + ' - ' + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + ''
              }
            },
            markers: {
              size: 0,
              hover: {
                sizeOffset: 6
              }
            },
            yaxis: {
              labels: {
                formatter: function (val) {
                  return (val);
                },
              },
              title: {
                text: 'Price'
              },
            },
            xaxis: {
              categories: result.date,
              type: 'datetime',
            },
            tooltip: {
              y: [
                {
                  title: {
                    formatter: function (val) {
                      return val + " $ "
                    }
                  }
                },
                {
                  title: {
                    formatter: function (val) {
                      return val + " $ "
                    }
                  }
                }
              ]
            },
            grid: {
              borderColor: '#f1f1f1',
            }
          },
          })
          console.log("result", result);
        })
}

SendToPredict = (data) => {
  if(!(data.open===0||data.close===0||data.volumn===0||data.open===''||data.close===''||data.volumn===''||data.open==='0'||data.close==='0'||data.volumn==='0')){
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ 'open':data.open,'close':data.close,'volumn':data.volumn })
    };
    fetch('/predictfuture', requestOptions)
        .then(response => response.json())
        .then(
          (result) => {
            // this.setState({
            //   series: [{
            //     name: "Real Price",
            //     data: result.realPrice
            //   },
            //   {
            //     name: "Predict Price",
            //     data: result.predictPrice
            //   }],
            //   currentTime:result.time,
            //   xaxis: {
            //     categories: result.date,
            //     type: 'datetime',
            //   },
            // });
            this.setState({
              predictFuture: result.predictPrice[0],
              
            
            })
            console.log("Giá trị dự đoán từ model chạy từ local server!!!!! ", result);
          })
        }
}

  componentWillMount() {
     this.loadQuote();
     fetch("/time")
     .then(res => res.json())
    .then(
      (result) => {
        // this.setState({
        //   series: [{
        //     name: "Real Price",
        //     data: result.realPrice
        //   },
        //   {
        //     name: "Predict Price",
        //     data: result.predictPrice
        //   }],
        //   currentTime:result.time,
        //   xaxis: {
        //     categories: result.date,
        //     type: 'datetime',
        //   },
        // });
        this.setState({
          scoreMSE: result.scoreTestMSE,
          scoreRMSE: result.scoreTestRMSE,
          series: [
                  {
                    name: "Predict Price",
                    data: result.predictPrice
                  },
                  {
                    name: "Real Price",
                            data: result.realPrice
                   },
          // {
          //   name: 'Total Visits',
          //   data: [87, 57, 74, 99, 75, 38, 62, 47, 82, 56, 45, 47]
          // }
        ],
        options: {
          chart: {
            height: 400,
            type: 'area',
            zoom: {
              type: 'x',
                enabled: true,
                autoScaleYaxis: true
            },
            toolbar: {
              autoSelected: 'zoom'
            }
          },
          dataLabels: {
            enabled: false
          },
          // stroke: {
          //   width: [5, 7, 5],
          //   curve: 'straight',
          //   dashArray: [0, 8, 5]
          // },
          title: {
            text: 'Stock Price Movement of Coca-Cola Company',
            align: 'left'
          },
          legend: {
            tooltipHoverFormatter: function(val, opts) {
              return val + ' - ' + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + ''
            }
          },
          markers: {
            size: 0,
            hover: {
              sizeOffset: 6
            }
          },
          yaxis: {
            labels: {
              formatter: function (val) {
                return (val);
              },
            },
            title: {
              text: 'Price'
            },
          },
          xaxis: {
            categories: result.date,
            type: 'datetime',
          },
          tooltip: {
            y: [
              {
                title: {
                  formatter: function (val) {
                    return val + " $ "
                  }
                }
              },
              {
                title: {
                  formatter: function (val) {
                    return val + " $ "
                  }
                }
              }
            ]
          },
          grid: {
            borderColor: '#f1f1f1',
          }
        },
        })
        console.log("result", result);
      })
  }
  

  loadQuote = () => {
    const {
      quote,
      enteredSymbol,
      quoteHistory,
      showHistory,
      news,
      chart,
      options,
      series,
    } = this.state;
    csv(url, data => (
      
      this.setState({
      chart:data
    }),
    this.updatedata()
    ))
    
  };
  updatedata = () => {
    const {
      quote,
      enteredSymbol,
      quoteHistory,
      showHistory,
      news,
      chart,
      options,
      series,
      score
    } = this.state;
    const chartCloses = [];
    const chartDates = [];
    chart.map((chartItem) => {
      chartDates.push(chartItem.date);
      chartCloses.push(chartItem.close);
      // // console.log("chart: ",chart);
      // console.log("chartDates: ",chartDates);
      // console.log("chartCloses: ",chartCloses);
      // // console.log("options: ",options);
      // // console.log("series: ",series);
      return null;
    })
    this.setState({
      series: [{
        name: "Real Price",
                data: chartCloses
              },
              {
                name: "Predict Price",
                data: chartCloses
              },
      // {
      //   name: 'Total Visits',
      //   data: [87, 57, 74, 99, 75, 38, 62, 47, 82, 56, 45, 47]
      // }
    ],
    options: {
      chart: {
        height: 400,
        type: 'area',
        zoom: {
          type: 'x',
            enabled: true,
            autoScaleYaxis: true
        },
        toolbar: {
          autoSelected: 'zoom'
        }
      },
      dataLabels: {
        enabled: false
      },
      // stroke: {
      //   width: [5, 7, 5],
      //   curve: 'straight',
      //   dashArray: [0, 8, 5]
      // },
      title: {
        text: 'Stock Price Movement of Coca-Cola Company',
        align: 'left'
      },
      legend: {
        tooltipHoverFormatter: function(val, opts) {
          return val + ' - ' + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + ''
        }
      },
      markers: {
        size: 0,
        hover: {
          sizeOffset: 6
        }
      },
      yaxis: {
        labels: {
          formatter: function (val) {
            return (val);
          },
        },
        title: {
          text: 'Price'
        },
      },
      xaxis: {
        categories: chartDates,
        type: 'datetime',
      },
      tooltip: {
        y: [
          {
            title: {
              formatter: function (val) {
                return val + " (mins)"
              }
            }
          },
          {
            title: {
              formatter: function (val) {
                return val + " per session"
              }
            }
          }
        ]
      },
      grid: {
        borderColor: '#f1f1f1',
      }
    },
    })
   
  };


  render() {
    // const [currentTime, setcurrentTime] = useState(1);
    const {
      quote,
      enteredSymbol,
      quoteHistory,
      showHistory,
      news,
      chart,
      options,
      series,
      scoreMSE,
      scoreRMSE,
      predictFuture
    } = this.state;
    const chartReverse = [...chart].reverse();
    const chartReverseMin = chartReverse.slice(0, 12);
    const quoteHistoryReverse = [...quoteHistory].reverse();

    const newsMin = [...news].slice(0, 2);

    const companyName = !!quote && quote.companyName;
    
    
    return ( 
    <div>
      <PredictField SendToPredict={this.SendToPredict} predictFuture={predictFuture}/>
      <SelectModel SendResquest={this.SendResquest} />
      <ReactApexChart options={options} series={series} type="area" height={400}/>
      <p>Score MSE : {scoreMSE} - Score RMSE : {scoreRMSE}</p>
      </div>
      )
  }
}

export default Predictdashboard;
