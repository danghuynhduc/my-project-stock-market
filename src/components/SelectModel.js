import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    marginLeft: '30%',
    marginRight: '10%',
    minWidth: 150,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));



export default function SelectModel (props) {
  const classes = useStyles();
  const [age, setAge] = React.useState('');

  const handleChange = (event) => {
    setAge(event.target.value);
    props.SendResquest(event.target.value);
  };

  return (
    <div className="row">
      <div className="col">
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-simple-select-helper-label">Name</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          value={age}
          onChange={handleChange}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={1}>Basic LSTM Model - 2 epochs</MenuItem>
          <MenuItem value={2}>Basic LSTM Model - 50 epochs</MenuItem>
          <MenuItem value={3}>Improved LSTM Model 1 - 50 epochs</MenuItem>
          <MenuItem value={4}>Improved LSTM Model 1 - 100 epochs</MenuItem>
          <MenuItem value={5}>Improved LSTM Model 2 - 50 epochs</MenuItem>
          <MenuItem value={6}>Improved LSTM Model 2 - 100 epochs</MenuItem>
          <MenuItem value={7}>Improved LSTM Model 3 - 50 epochs</MenuItem>
          <MenuItem value={8}>Improved LSTM Model 3 - 100 epochs</MenuItem>
          
        </Select>
        <FormHelperText>Tên Model cùng thuộc tính đã được train</FormHelperText>
      </FormControl>
      </div>
      <div className="col">
      <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel id="demo-simple-select-outlined-label">Model</InputLabel>
        <Select
          labelId="demo-simple-select-outlined-label"
          id="demo-simple-select-outlined"
          value={age}
          onChange={handleChange}
          label="Age"
        >
          <MenuItem value="">
            <em>Default</em>
          </MenuItem>
          <MenuItem value={1}>One</MenuItem>
          <MenuItem value={2}>Two</MenuItem>
          <MenuItem value={3}>Three</MenuItem>
          <MenuItem value={4}>Four</MenuItem>
          <MenuItem value={5}>Five</MenuItem>
          <MenuItem value={6}>Six</MenuItem>
          <MenuItem value={7}>Sevent</MenuItem>
          <MenuItem value={8}>Eight</MenuItem>
        </Select>
      </FormControl>
      </div>
    </div>
  );
}
