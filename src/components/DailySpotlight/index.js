import React from "react";
import { json } from "d3";
import StatisticsTable from "./StatisticsTable";
import MoneyFlowsTable from "./MoneyFlowsTable";

import styled from "styled-components";
import Grid from "@material-ui/core/Grid";
import * as d3 from "d3";

const fetchMoneyFlows = async (url) => {
  const file = await fetch(url);
  let response = await file.text();
  let data = response.trim();
  data = data.substring(data.indexOf("C"));
  data = d3.tsvParse(data, d3.autoType);
  return data;
};

const StyledSpotlight = styled.div`
  .moneyFlowTables {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }

  .moneyflowTable {
    /* // width:40%; */
  }
`;

/*
class MoneyFlows extends React.Component {
  state = {
    fetchedBuyingOnWeakness: false,
    fetchedSellingOnStrength: false,
    buyingOnWeakness: [],
    sellingOnStrength: []
  };

  componentDidMount() {
    this.getSellingOnStrength();
    this.getBuyingOnWeakness();
  }


  getSellingOnStrength = async () => {
    const data = await fetchMoneyFlows(
      "http://cors-anywhere.herokuapp.com/online.wsj.com/mdc/public/npage/2_3045-mflppg-mfxml2csv.html"
    );

    this.setState({
      fetchedSellingOnStrength: true,
      sellingOnStrength: data
    });
  };
  getBuyingOnWeakness = async () => {
    const data = await fetchMoneyFlows(
      "http://cors-anywhere.herokuapp.com/online.wsj.com/mdc/public/npage/2_3045-mfgppl-mfxml2csv.html"
    );

    this.setState({
      fetchedBuyingOnWeakness: true,
      buyingOnWeakness: data
    });
    console.log(data);
  };


render()
{
   return (
      <StyledSpotlight>
        <div className='spotlight'>
        <Grid columns={3} relaxed='very' divided stackable>
      <Grid.Column>
              
                {this.state.fetchedSellingOnStrength ? (
                  <MoneyFlowsTable
                    className='sellingOnStrength'
                    key='SellingFlow'
                    name='Selling Strength'
                    color='red'
                    data={this.state.sellingOnStrength}
                    icon='arrow right'
                     onClick={evt=>evt.stopPropogration}
                  />
                ) : (
                  <h1> Loading</h1>
                )}
         
            </Grid.Column>
            <Grid.Column>
              
                {this.state.fetchedBuyingOnWeakness ? (
                  <MoneyFlowsTable
                    className='buyingOnWeakness'
                    key='BuyingFlow'
                    name='Buying Weakness'
                    color='green'
                    data={this.state.buyingOnWeakness}
                    icon='arrow left'
                    // onClick={evt=>evt.stopPropogration}
                  />
                ) : (
                  <h1> Loading</h1>
                )}
           
            </Grid.Column></Grid>        </div>
      </StyledSpotlight>)

}

}

*/
class Spotlight extends React.Component {
  state = {
    fetchedGainers: false,
    fetchedLosers: false,
    fetchedActive: false,
    gainers: [],
    losers: [],
    active: [],
  };
  componentDidMount() {
    this.getGainers();
    this.getLosers();
    this.getActive();
  }

  getGainers = async () => {
    const data = await json(
      "https://cloud.iexapis.com/stable/stock/market/list/gainers?token=pk_930da6c1c50b4e33914febac3ab39fcb"
    );
    var emm = [];
    for (var i = 0; i < 5; i++) emm.push(data[i]);
    this.setState({
      fetchedGainers: true,
      gainers: emm,
    });
  };
  getLosers = async () => {
    const data = await json(
      "https://cloud.iexapis.com/stable/stock/market/list/losers?token=pk_930da6c1c50b4e33914febac3ab39fcb"
    );
    var emm = [];
    for (var i = 0; i < 5; i++) emm.push(data[i]);
    this.setState({
      fetchedLosers: true,
      losers: emm,
    });
  };

  getActive = async () => {
    const data = await json(
      "https://cloud.iexapis.com/stable/stock/market/list/mostactive?token=pk_930da6c1c50b4e33914febac3ab39fcb"
    );
    this.setState({
      fetchedActive: true,
      active: data,
    });
  };
  render() {
    return (
      <div>
        <div className="spotlight">
          <Grid container spacing={5} justify="center">
            <Grid item xs={4}>
              <Grid>
                {this.state.fetchedGainers ? (
                  <StatisticsTable
                    key="TopGainers"
                    name="Top Công Ty Lãi"
                    color="green"
                    data={this.state.gainers}
                    icon="arrow up"
                  />
                ) : (
                  <h1> Loading</h1>
                )}
              </Grid>
              <Grid style={{ marginTop: "2rem" }}>
                {this.state.fetchedLosers ? (
                  <StatisticsTable
                    key="TopLosers"
                    name="Top Công Ty Lỗ"
                    color="red"
                    data={this.state.losers}
                    icon="arrow down"
                  />
                ) : (
                  <h1> Loading</h1>
                )}
              </Grid>
            </Grid>
            <Grid item xs={6} justify="center">
              <Grid>
                {this.state.fetchedActive ? (
                  <StatisticsTable
                    key="MostActive"
                    name="Top Công Ty nhiều giao dịch"
                    color="black"
                    inverted
                    data={this.state.active}
                    icon="bolt"
                  />
                ) : (
                  <h1> Loading</h1>
                )}
              </Grid>
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}

export default Spotlight;
