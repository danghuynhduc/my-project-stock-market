import React from "react";
import styled from "styled-components";
// TRADING-DAY COMPONENTS
import Clock from "react-live-clock";

const HeaderStyles = styled.div`
  user-select: none;
  @import url(https://fonts.googleapis.com/css?family=Arvo:700);
  @import url(https://fonts.googleapis.com/css?family=Seaweed+Script);
  .col {
    text-align: center;
    background: white;
    color: #000;
    letter-spacing: 0.5rem;
    color: black;
    font-weight: 500;
    font-size: 1.5rem;
    border-radius: 0.25em;
    line-height: 2rem;
    align-self: center;
  }
  .row{
    margin:0;
  }

`;

export default (Header) => {
  return (
    <HeaderStyles>
      <div  className="row">
        <div className="col">
          <Clock
            format={"[VN -] HH:mm:ss a"}
            ticking={true}
            timezone={"Asia/Ho_Chi_Minh"}
            interval={1000}
          />
        </div>
        <div className="col">
          <Clock
            format={"[USA -] HH:mm:ss a"}
            ticking={true}
            timezone={"America/Los_Angeles"}
            interval={1000}
          />
        </div>
        </div>
    </HeaderStyles>
  );
};
