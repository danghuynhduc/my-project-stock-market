import React,{Component} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

// const useStyles = makeStyles((theme) => ({
//   formControl: {
//     margin: theme.spacing(1),
//     marginLeft: '30%',
//     marginRight: '10%',
//     minWidth: 150,
//   },
//   selectEmpty: {
//     marginTop: theme.spacing(2),
//   },
// }));



export default class PredictField extends Component{
  constructor(props) {
    super(props);
    this.state = {
      open: 0,
      close: 0,
      volumn:0
    };
  }

  handle = () => {
    this.props.SendToPredict(this.state);
  };

  handleChanges = (e) => {
    this.setState({ [e.currentTarget.id]: e.currentTarget.value });
  };
render (){
  
  return (
    <div className="row" style={{"margin" : 10}}>
      <div className="col">
      <TextField  onChange={this.handleChanges} id="open" required label="Open" variant="outlined" />
      </div>
      <div className="col">
      <TextField  onChange={this.handleChanges} id="close" required label="Close" variant="outlined" />
      </div>
      <div className="col">
        <TextField  onChange={this.handleChanges} id="volumn" required label="Volumn" variant="outlined" />
      </div>
      <div className="col">
      <Button onClick={this.handle} variant="outlined" color="primary">
        Submit Predict
      </Button>
      </div>
      <div className="col">
        <TextField disabled  id="result" label="Result" variant="filled" value={this.props.predictFuture} />
      </div>
    </div>
  );
}
}

