import React, { Component } from "react";
import { BrowserRouter, Link } from "react-router-dom";
import "./App.css";
import styled from "styled-components";
import StockInfo from "./components/StockInfo";
import NewsList from "./components/NewsList";
import ChartLineGraph from "./components/ChartLineGraph";
import ChartTable from "./components/ChartTable";

import Header from "./components/Header";
import Routes from "./routes";

import Home from "./routes/Home";
import SearchTraningCompany from "./routes/SearchTraningCompany";

import {
  loadQuotesForStock,
  loadLogoForStock,
  loadRecentNewsForStock,
  loadChartForStock,
} from "./api/iex";

const AppWrapper = styled.div``;
class App extends Component {
  state = {
    error: null,
    enteredSymbol: "aapl",
    quote: null,
    quoteHistory: [],
    showHistory: false,
    news: [],
    showAllNews: false,
    chart: [],
    showAllChart: false,
  };

  componentDidMount() {
    this.loadQuote();
  }
  // The first time our component is rendered
  // this method is called
  // componentDidMount() {
  //   this.loadQuote();
  // }

  loadQuote = () => {
    const { enteredSymbol } = this.state;
  };

  onChangeEnteredSymbol = (event) => {
    // The <input> text value entered by user
    // No Spaces, Upper case, Limited to 4 chars
    const value = event.target.value.trim().toUpperCase().slice(0, 4);
    // Change this.state.enteredSymbol
    this.setState({
      enteredSymbol: value,
    });
  };

  onKeyDownPressEnter = (event) => {
    if (event.keyCode === 13) {
      this.loadQuote();
    }
  };

  onClickShowHistory = (event) => {
    this.setState((prevState) => {
      const showHistory = prevState.showHistory;
      return {
        showHistory: !showHistory,
      };
    });
  };

  onClickShowAllChart = (event) => {
    this.setState((prevState) => {
      const showAllChart = prevState.showAllChart;
      return {
        showAllChart: !showAllChart,
      };
    });
  };

  onClickShowAllNews = (event) => {
    this.setState((prevState) => {
      const showAllNews = prevState.showAllNews;
      return {
        showAllNews: !showAllNews,
      };
    });
  };

  render() {
    const { quote, quoteHistory, news, chart } = this.state;
    console.log("chart", chart);
    const chartReverse = [...chart].reverse();
    console.log("chartReverse", chartReverse);
    const chartCloses = [];
    const chartDates = [];
    chart.map((chartItem) => {
      chartDates.push(chartItem.label);
      chartCloses.push(chartItem.close);
      return null;
    });
    return (
      <AppWrapper>
        <Header />
        <BrowserRouter>
          <div>
            <div
              className="jumbotron jumbotron-fluid bg-dark text-light"
              style={{ marginBottom: 0, paddingBottom: "3rem" }}
            >
              <div className="container">
                <h1 className="display-3">Giao dịch chứng khoán</h1>

                <Link to="/SearchTraningCompany">
                  <p className="lead">Tra cứu công ty/thương hiệu</p>
                </Link>
              </div>
            </div>
          </div>

          <Routes />
        </BrowserRouter>
      </AppWrapper>
    );
  }
}

export default App;
