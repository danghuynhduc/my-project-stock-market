import axios from "axios";
import { api_token } from "../constants/api_token";
const api = axios.create({
  baseURL: "https://cloud.iexapis.com/stable",
});

export const loadQuotesForStock = (symbol) => {
  return api
    .get(`/stock/${symbol}/quote?token=${api_token}`)
    .then((res) => res.data);
};

export const loadLogoForStock = (symbol) => {
  return api
    .get(`/stock/${symbol}/logo?token=${api_token}`)
    .then((res) => res.data.url);
};

export const loadRecentNewsForStock = (symbol) => {
  return api
    .get(`/stock/${symbol}/news?token=${api_token}`)
    .then((res) => res.data);
};

export const loadChartForStock = (symbol, range) => {
  return api
    .get(`/stock/${symbol}/chart/${range}?token=${api_token}`)
    .then((res) => res.data);
};
