import math
from keras.models import load_model
import stock_data as sd
import pandas as pd
from IPython.display import display
import visualize as vs

stocks = pd.read_csv('F:\\Stock Market for Project 2\\my-project-stock-market\\applicationAIinStockMarket\\data_process\\coca-cola_preprocessed.csv')
stocks_data = stocks.drop(['Item'], axis =1)

display(stocks_data.head())

X_train, X_test,y_train, y_test = sd.train_test_split_lstm(stocks_data, 15)

unroll_length = 50
X_train = sd.unroll(X_train, unroll_length)
X_test = sd.unroll(X_test, unroll_length)
y_train = y_train[-X_train.shape[0]:]
y_test = y_test[-X_test.shape[0]:]

model = load_model('coca-cola_traning_model_basicf15d.h5')
model.summary()

predictions = model.predict(X_test)
predictions = model.predict(X_test)
print(predictions[-15:])
print(y_test[-15:])
arr=[]
for i in range(15,0,-1):
    arr.append(100-abs(predictions[-i]-y_test[-i])*100/y_test[-i])
print("H of array: ")
for ar in arr:
    print(ar)
vs.plot_lstm_prediction(y_test,predictions)

trainScore = model.evaluate(X_train, y_train, verbose=0)
print('Train Score: %.8f MSE (%.8f RMSE)' % (trainScore, math.sqrt(trainScore)))

testScore = model.evaluate(X_test, y_test, verbose=0)
print('Test Score: %.8f MSE (%.8f RMSE)' % (testScore, math.sqrt(testScore)))