import math
import pandas as pd
import numpy as np
from IPython.display import display
from sklearn import linear_model
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import TimeSeriesSplit

import visualize as vs
import stock_data as sd
import LinearRegressionModel

stocks = pd.read_csv('F:\\Stock Market for Project 2\\my-project-stock-market\\applicationAIinStockMarket\\data_process\\coca-cola_preprocessed.csv')
display(stocks.head())

X_train, X_test, y_train, y_test, label_range= sd.train_test_split_linear_regression(stocks)

print("x_train", X_train)
print("y_train", y_train)
print("x_test", X_test)



model = LinearRegressionModel.build_model(X_train,y_train)

predictions = LinearRegressionModel.predict_prices(model,X_test, label_range)
print("y_test", y_test)
print("predictions", predictions)
testScore = model.score(X_test, y_test)
print("testScore", testScore)

#vs.show(y_test,"Giá trị thực của cổ phiếu Coca-Cola trong giai đoạn xét")
vs.plot_prediction(y_test,predictions," Linear Regression Model Predict ")