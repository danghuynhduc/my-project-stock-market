import urllib3,json
http = urllib3.PoolManager()

token ='pk_930da6c1c50b4e33914febac3ab39fcb'
prefix_url = 'https://cloud.iexapis.com/stable'


# symbols = ['aapl','msft','goog','fb','baba','tsla','wmt','ma','unh',
# 'dis','bac','nvda','pypl','intc','nflx','orcl','mcd','ba','sne',
# 'sbux','tot','tm','aig','ko','ebay','nok','ibm']
symbols = ['tsla','dis','ko','nok']
company = ['tesla','disney','coca-cola','nokia']
range_time = '2d'
# range_time = '15y'
# raw_datas = []
# for symbol in symbols:
#     suffix_url = '/stock/'+ symbol +'/chart/'+range+'?token='
#     url = prefix_url+suffix_url+token
#     response = http.request('GET',url)
#     raw_datas.append(json.loads(response.data))

#
#Code phía trên get data từ iex.com lưu vào biến raw_datas
#

#Dữ liệu ban đầu có dạng json
#

#Clean raw_datas thành data sạch chứa các phần tử là data của các công ty trong 'symbols'
#labels = ['symbol','date','open','close','high','low','volume']
labels = ['date','open','close','volume']
#Ghi data vào file text 'stock_market_data.csv'
# open("stock_market_data.csv","w")
# with open("stock_market_data.csv","a") as file:

# Xử lý về dạng symbol,date,open,close,high,low,volume

# open("test_get_data.txt","w")
# with open("test_get_data.txt","a") as file:
#     for label in labels:
#         file.write(label +',' )
#     file.write('\n')
#     for raw_data in raw_datas:
#         for rd in raw_data:
#             for label in labels:
#                 if(label!=labels[-1]):
#                     file.write(str(rd[label])+',')
#                 else:
#                     file.write(str(rd[label]))

#             file.write('\n')
for i in range(len(symbols)):
    open('test_get'+company[i]+".csv","w")
    raw_datas = []
    suffix_url = '/stock/'+ symbols[i] +'/chart/'+range_time+'?token='
    url = prefix_url+suffix_url+token
    response = http.request('GET',url)
    raw_datas.append(json.loads(response.data))
    with open('test_get'+company[i]+".csv","a") as file:
        for label in labels:
            if(label!=labels[-1]):
                file.write(label +',' )
            else:
                 file.write(label)
        file.write('\n')
        for raw_data in raw_datas:
            for rd in raw_data:
                for label in labels:
                    if(label!=labels[-1]):
                        file.write(str(rd[label])+',')
                    else:
                        file.write(str(rd[label]))

                file.write('\n')