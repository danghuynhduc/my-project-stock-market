from keras.models import load_model
import math
import pandas as pd
import numpy as np
from IPython.display import display

from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from keras.metrics import mean_squared_error
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import MinMaxScaler
import lstm, time #helper libraries

import visualize as vs
import stock_data as sd


stocks = pd.read_csv('F:\\Stock Market for Project 2\\my-project-stock-market\\applicationAIinStockMarket\\data_process\\coca-cola_preprocessed.csv')
stocks_data = stocks.drop(['Item'], axis =1)

display(stocks_data.head())

X_train, X_test,y_train, y_test = sd.train_test_split_lstm(stocks_data, 1, 1200)

unroll_length = 50
X_train = sd.roll(X_train, unroll_length)
X_test = sd.roll(X_test, unroll_length)
y_train = y_train[-X_train.shape[0]:]
y_test = y_test[-X_test.shape[0]:]

# predictions = model.predict(X_test)

# print("Xtrain: ",X_train[0:5])
# print("ytrain: ",y_train[0:5])
# print("Xtest: ",X_test[0:5])
# print("predict : ",predictions[0:5])
# print("yTest: ",y_test[0:5])

# print(predictions[-5:])
# print(y_test[-5:])
# arr=[]
# for i in range(5,0,-1):
#     arr.append(100-abs(predictions[-i]-y_test[-i])*100/y_test[-i])
# print("H of array: ")
# for ar in arr:
#     print(ar)
# a = np.array([1,2,3,4])
# print("a",a)
# b=a.reshape(1,-1)
# print("b",b)


# print("pre_test[0]",X_test)
a=X_test[0]
print("a",a)
v=[]
v.append(a)
v=np.array(v)
u=[]
u.append(v)
c = np.array(u)
print("DDDDDD",X_test)
print("DDDDDD",v)
b = []
b.append(X_test[0])
b=np.array(b)
print("b",b)
print("X_test",X_test)

scaler = MinMaxScaler()
scaler2 = MinMaxScaler()
root_data = pd.read_csv('coca-cola.csv')
numerical = ['open', 'close', 'volume']
numeri = ['close']
err  = scaler2.fit_transform(root_data[numeri])
print("err",err)
a = scaler.fit_transform(root_data[numerical])

test = np.array([[127.82, 130.48, 96906490]])

# test.reshape(-1,1)
test = scaler.transform(test)
bk = []
bk.append(test)
bk = np.array(bk)

print(bk)
# bk = scaler.transform(bk)
# print("bk",bk)
model = load_model('testModel.h5')
# model.summary()
predictions = model.predict(bk)
print("pre_test",predictions)
predictions=scaler2.inverse_transform(predictions)
print("pre_test",predictions)

# vs.plot_lstm_prediction(y_test,predictions,'Improve LSTM Model - 3 Predict')

# trainScore = model.evaluate(X_train, y_train, verbose=0)
# print('Train Score: %.8f MSE (%.8f RMSE)' % (trainScore, math.sqrt(trainScore)))

# testScore = model.evaluate(X_test, y_test, verbose=0)
# print('Test Score: %.8f MSE (%.8f RMSE)' % (testScore, math.sqrt(testScore)))