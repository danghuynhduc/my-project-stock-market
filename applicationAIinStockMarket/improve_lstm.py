import math
import pandas as pd
import numpy as np
from IPython.display import display
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from keras.metrics import mean_squared_error
from sklearn.model_selection import StratifiedKFold

import lstm, time #helper libraries

import visualize as vs
import stock_data as sd
import LinearRegressionModel

stocks = pd.read_csv('F:\\Stock Market for Project 2\\my-project-stock-market\\applicationAIinStockMarket\\data_process\\coca-cola_preprocessed.csv')
stocks_data = stocks.drop(['Item'], axis =1)

display(stocks_data.head())

X_train, X_test,y_train, y_test = sd.train_test_split_lstm(stocks_data, 1, 1200)

unroll_length = 50
X_train = sd.roll(X_train, unroll_length)
X_test = sd.roll(X_test, unroll_length)
y_train = y_train[-X_train.shape[0]:]
y_test = y_test[-X_test.shape[0]:]

print("x_train", X_train.shape)
print("y_train", y_train.shape)
print("x_test", X_test.shape)
print("y_test", y_test.shape)
#
# build basic lstm model

# Set up hyperparameters
batch_size = 50
epochs = 100

# build improved lstm model
# model = lstm.build_improved1_model( X_train.shape[-1],output_dim = unroll_length, return_sequences=True)
model = lstm.build_improved2_model()
# model = lstm.build_improved3_model( X_train.shape[-1],output_dim = unroll_length, return_sequences=True)
start = time.time()
#final_model.compile(loss='mean_squared_error', optimizer='adam')
model.compile(loss='mean_squared_error', optimizer='adam')
print('compilation time : ', time.time() - start)

# Compile the model


# # Compile the model
# model.compile(optimizer='adam', loss='mean_squared_error')

model.fit(X_train,
          y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=2,
          validation_split=0.05
         )


# Generate predictions 
predictions = model.predict(X_test, batch_size=batch_size)

print(predictions[-5:])
print(y_test[-5:])
arr=[]
for i in range(5,0,-1):
    arr.append(100-abs(predictions[-i]-y_test[-i])*100/y_test[-i])
print("H of array: ")
for ar in arr:
    print(ar)

vs.plot_lstm_prediction(y_test,predictions,"Improved Model LSTM - 3")

trainScore = model.evaluate(X_train, y_train, verbose=0)
print('Train Score: %.8f MSE (%.8f RMSE)' % (trainScore, math.sqrt(trainScore)))

testScore = model.evaluate(X_test, y_test, verbose=0)
print('Test Score: %.8f MSE (%.8f RMSE)' % (testScore, math.sqrt(testScore)))
# from keras_contrib.layers import CRF
# from keras_contrib.losses import crf_loss
# from keras_contrib.metrics import crf_viterbi_accuracy
# custom_objects={'CRF': CRF,
#                         'crf_loss': crf_loss,
#                         'crf_viterbi_accuracy': crf_viterbi_accuracy}
model.save("testModel.h5")

print("Saved model to disk")
