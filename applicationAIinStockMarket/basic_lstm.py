import math
import pandas as pd
import numpy as np
from IPython.display import display
import tensorflow as tf
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from keras.metrics import mean_squared_error
from sklearn.model_selection import StratifiedKFold

import lstm, time #helper libraries

import visualize as vs
import stock_data as sd
import LinearRegressionModel

stocks = pd.read_csv('F:\\Stock Market for Project 2\\my-project-stock-market\\applicationAIinStockMarket\\data_process\\coca-cola_preprocessed.csv')
stocks_data = stocks.drop(['Item'], axis =1)

display(stocks_data.head())

X_train, X_test,y_train, y_test = sd.train_test_split_lstm(stocks_data, 1, 1200)

unroll_length = 50
X_train = sd.unroll(X_train, unroll_length)
X_test = sd.unroll(X_test, unroll_length)
y_train = y_train[-X_train.shape[0]:]
y_test = y_test[-X_test.shape[0]:]

print(np.__version__)
print(tf.__version__)
print("x_train", X_train.shape)
print("y_train", y_train.shape)
print("x_test", X_test.shape)
print("y_test", y_test.shape)



# build basic lstm model
model = lstm.build_basic_model(input_dim = X_train.shape[-1],output_dim = unroll_length, return_sequences=False)

# Compile the model
start = time.time()
model.compile(loss='mean_squared_error', optimizer='adam')
print('compilation time : ', time.time() - start)

model.fit(
    X_train,
    y_train,
    epochs=2,
    validation_split=0.05)

predictions = model.predict(X_test)
predictions = model.predict(X_test)
print(predictions[-15:])
print(y_test[-15:])
arr=[]
for i in range(15,0,-1):
    arr.append(100-abs(predictions[-i]-y_test[-i])*100/y_test[-i])
print("H of array: ")
for ar in arr:
    print(ar)
vs.plot_lstm_prediction(y_test,predictions,"Basic LSTM Model Predict ")

trainScore = model.evaluate(X_train, y_train, verbose=0)
print('Train Score: %.8f MSE (%.8f RMSE)' % (trainScore, math.sqrt(trainScore)))

testScore = model.evaluate(X_test, y_test, verbose=0)
print('Test Score: %.8f MSE (%.8f RMSE)' % (testScore, math.sqrt(testScore)))

model.save("basic_lstm_2epochs.h5")

print("Saved model to disk")
