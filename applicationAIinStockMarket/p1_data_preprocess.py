import pandas as pd
import numpy as np

file_raw_data = ['coca-cola.csv','disney.csv','nokia.csv','tesla.csv']
titles_file = ['coca-cola_preprocessed.csv','disney_preprocessed.csv','nokia_preprocessed.csv','tesla_preprocessed.csv']
company = ['Coca Cola','Disney','Nokia','Tesla']
index = 3
#
#Load raw_data
data = pd.read_csv('F:\\Stock Market for Project 2\\my-project-stock-market\\applicationAIinStockMarket\\raw_data\\'+file_raw_data[index])
# print(data.head())
#
#Preprocessed
import preprocess_data as ppd
stocks = ppd.remove_data(data)

# print(stocks.head())
# print("----")
# print(stocks.tail())

# #Visual real_traning_data
import visualize as vs
vs.plot_basic(stocks,company[index]+" Training")
# 
stocks = ppd.get_normalised_data(stocks)
print(stocks)

stocks.to_csv(titles_file[index],index=False)